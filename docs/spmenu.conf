/* spmenu default configuration file
 *
 * Copy to ~/.config/spmenu/spmenu.conf to use.
 * Note that you can @include other config files if you want
 *
 * Example: @include "config.conf"
 */
spmenu =
{
  // General window options
  window = ( { position = 1;
               border = 0;
               margin-vertical = 0;
               margin-horizontal = 0;
               padding-vertical = 0;
               padding-horizontal = 0;
               x = 0;
               y = 0;
               width = 0;
               monitor = -1;
               managed = 0;
               alpha = 1;
           } );

  // Properties
  properties = ( { class = "spmenu";
                   dock = 1;
           } );

  // Text
  text = ( { font = "Noto Sans Mono 8";
             padding = 0;
             leftarrow = "<";
             rightarrow = ">";
             password = "*";
             prompt = "";
             input = "";
             capslockon = "Caps Lock";
             capslockoff = "";
           } );

  // Color
  color = ( { itemnormfg = "#bbbbbb";
              itemnormbg = "#222222";
              itemselfg = "#eeeeee";
              itemselbg = "#35638A";
              itemnormprifg = "#bbbbbb";
              itemnormpribg = "#222222";
              itemselprifg = "#eeeeee";
              itemselpribg = "#35638A";
              inputfg = "#eeeeee";
              inputbg = "#222222";
              menu = "#222222";
              promptfg = "#eeeeee";
              promptbg = "#35526b";
              larrowfg = "#bbbbbb";
              larrowbg = "#222222";
              rarrowfg = "#bbbbbb";
              rarrowbg = "#222222";
              hlnormfg = "#ffffff";
              hlnormbg = "#000000";
              hlselfg = "#ffffff";
              hlselbg = "#000000";
              numfg = "#ffffff";
              numbg = "#2d3856";
              modefg = "#ffffff";
              modebg = "#35638A";
              capsfg = "#ffffff";
              capsbg = "#45638A";
              border = "#35638A";
              caretfg = "#ffffff";
              caretbg = "#222222";
              sgr0 = "#000000";
              sgr1 = "#7f0000";
              sgr2 = "#007f00";
              sgr3 = "#7f7f00";
              sgr4 = "#00007f";
              sgr5 = "#7f007f";
              sgr6 = "#007f7f";
              sgr7 = "#cccccc";
              sgr8 = "#333333";
              sgr9 = "#ff0000";
              sgr10 = "#00ff00";
              sgr11 = "#ffff00";
              sgr12 = "#0000ff";
              sgr13 = "#ff00ff";
              sgr14 = "#00ffff";
              sgr15 = "#ffffff";
              coloritems = 1;
              sgr = 1;
            } );

  // Alpha options
  alpha = ( { itemnormfg = 255;
              itemnormbg = 200;
              itemselfg = 255;
              itemselbg = 200;
              itemnormprifg = 255;
              itemnormpribg = 200;
              itemselprifg = 255;
              itemselpribg = 200;
              inputfg = 255;
              inputbg = 200;
              menu = 200;
              promptfg = 255;
              promptbg = 200;
              larrowfg = 255;
              larrowbg = 200;
              rarrowfg = 255;
              rarrowbg = 200;
              hlnormfg = 255;
              hlnormbg = 200;
              hlselfg = 255;
              hlselbg = 200;
              numfg = 255;
              numbg = 200;
              border = 255;
              caretfg = 255;
              caretbg = 200;
              modefg = 255;
              modebg = 200;
              capsfg = 255;
              capsbg = 200;
        } );

  // Powerline options
  powerline = ( { promptstyle = 0;
                  matchcountstyle = 0;
                  modestyle = 0;
                  capsstyle = 0;
                  prompt = 1;
                  matchcount = 1;
                  mode = 1;
                  caps = 1;
            } );

  // Hide options
  hide = ( { input = 0;
             larrow = 0;
             rarrow = 0;
             items = 0;
             prompt = 0;
             powerline = 0;
             caret = 0;
             highlight = 0;
             matchcount = 0;
             mode = 0;
             caps = 0;
             image = 0;
            } );

  // Match options
  match = ( { sort = 1;
              casesensitive = 0;
              fuzzy = 1;
              preselected = 0;
              accuratewidth = 1;
              delimiters = " ";
           } );

  // Line options
  line = ( { height = 1;
             lines = 0;
             columns = 10;
             indentitems = 1;
           } );

  // History options
  history = ( { max = 64;
                duplicate = 0;
            } );

  // Centered menu
  center = ( { width = 1000;
           } );

  // Image
  image = ( { width = 200;
              height = 200;
              gaps = 0;
              position = 0;
              cache = 1;
              maxcache = 512;
           } );

  // Xrdb
  xrdb = ( { xresources = 1;
             global = 1;
           } );

  // Input
  input = ( { fast = 1;
              type = 1;
              password = 0;
           } );

  // Mode
  mode = ( { default = 0;
             normal_text = "Normal";
             insert_text = "Insert";
           } );

  // Pango
  pango = ( { item = 1;
              highlight = 1;
              prompt = 1;
              input = 1;
              leftarrow = 0;
              rightarrow = 0;
              numbers = 0;
              mode = 0;
              caps = 0;
              password = 0;
            } );

  // Mouse
  mouse = ( { click = "clickinput";
              modifier = "None";
              button = "Left Click";
              function = "clear";
              argument = "0";
            },
            { click = "clickprompt";
              modifier = "None";
              button = "Left Click";
              function = "clear";
              argument = "0";
            },
            { click = "clickmode";
              modifier = "None";
              button = "Left Click";
              function = "switchmode";
              argument = "0";
            },
            { click = "clicknumber";
              modifier = "None";
              button = "Left Click";
              function = "viewhist";
              argument = "0";
            },
            { click = "clickselitem";
              modifier = "None";
              button = "Left Click";
              function = "None";
              argument = "0";
            },

            { ignoreglobalmouse = 1; } );

  // Keys
  keys = ( { mode = -1;
             modifier = "None";
             key = "Enter";
             function = "selectitem";
             argument = "+1";
           },
           { mode = -1;
             modifier = "Shift";
             key = "Enter";
             function = "selectitem";
             argument = "0";
           },
           { mode = -1;
             modifier = "None";
             key = "Tab";
             function = "complete";
             argument = "0";
           },
           { mode = -1;
             modifier = "Ctrl";
             key = "v";
             function = "paste";
             argument = "2";
           },
           { mode = -1;
             modifier = "Ctrl+Shift";
             key = "v";
             function = "paste";
             argument = "1";
           },
           { mode = -1;
             modifier = "None";
             key = "Backspace";
             function = "backspace";
             argument = "0";
           },
           { mode = -1;
             modifier = "Ctrl";
             key = "Backspace";
             function = "deleteword";
             argument = "0";
           },
           { mode = -1;
             modifier = "Ctrl";
             key = "Left";
             function = "moveword";
             argument = "-1";
           },
           { mode = -1;
             modifier = "Ctrl";
             key = "Right";
             function = "moveword";
             argument = "+1";
           },
           { mode = -1;
             modifier = "None";
             key = "Left";
             function = "movecursor";
             argument = "-1";
           },
           { mode = -1;
             modifier = "None";
             key = "Right";
             function = "movecursor";
             argument = "+1";
           },
           { mode = -1;
             modifier = "Ctrl+Shift";
             key = "p";
             function = "setprofile";
             argument = "0";
           },
           { mode = 1;
             modifier = "None";
             key = "Esc";
             function = "switchmode";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "i";
             function = "switchmode";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "/";
             function = "switchmode";
             argument = "0";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "=";
             function = "setimgsize";
             argument = "+1";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "-";
             function = "setimgsize";
             argument = "-1";
           },
           { mode = 0;
             modifier = "None";
             key = "-";
             function = "setimgsize";
             argument = "-10";
           },
           { mode = 0;
             modifier = "None";
             key = "=";
             function = "setimgsize";
             argument = "+10";
           },
           { mode = 0;
             modifier = "Shift";
             key = "-";
             function = "setimgsize";
             argument = "-100";
           },
           { mode = 0;
             modifier = "Shift";
             key = "=";
             function = "setimgsize";
             argument = "+100";
           },
           { mode = 0;
             modifier = "Shift";
             key = "0";
             function = "defaultimg";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "r";
             function = "rotateimg";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "p";
             function = "setimgpos";
             argument = "+1";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "1";
             function = "setimggaps";
             argument = "-1";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "2";
             function = "setimggaps";
             argument = "+1";
           },
           { mode = 0;
             modifier = "None";
             key = "1";
             function = "setimggaps";
             argument = "-10";
           },
           { mode = 0;
             modifier = "None";
             key = "2";
             function = "setimggaps";
             argument = "+10";
           },
           { mode = 0;
             modifier = "Shift";
             key = "1";
             function = "setimggaps";
             argument = "-100";
           },
           { mode = 0;
             modifier = "Shift";
             key = "2";
             function = "setimggaps";
             argument = "+100";
           },
           { mode = 0;
             modifier = "None";
             key = "t";
             function = "toggleimg";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "h";
             function = "flipimg";
             argument = "1";
           },
           { mode = 0;
             modifier = "None";
             key = "v";
             function = "flipimg";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "k";
             function = "moveup";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "j";
             function = "movedown";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "h";
             function = "moveleft";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "l";
             function = "moveright";
             argument = "0";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "u";
             function = "moveup";
             argument = "5";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "d";
             function = "movedown";
             argument = "5";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "k";
             function = "setlines";
             argument = "+1";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "j";
             function = "setlines";
             argument = "-1";
           },
           { mode = 0;
             modifier = "Ctrl+Alt+Shift";
             key = "k";
             function = "setlines";
             argument = "+5";
           },
           { mode = 0;
             modifier = "Ctrl+Alt+Shift";
             key = "j";
             function = "setlines";
             argument = "-5";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "h";
             function = "setcolumns";
             argument = "+1";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "h";
             function = "setcolumns";
             argument = "-1";
           },
           { mode = 0;
             modifier = "Ctrl+Alt+Shift";
             key = "h";
             function = "setcolumns";
             argument = "+5";
           },
           { mode = 0;
             modifier = "Ctrl+Alt+Shift";
             key = "l";
             function = "setcolumns";
             argument = "-5";
           },
           { mode = 0;
             modifier = "Ctrl";
             key = "k";
             function = "restoresel";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "u";
             function = "togglehighlight";
             argument = "0";
           },
           { mode = 0;
             modifier = "Ctrl+Shift";
             key = "h";
             function = "viewhist";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "d";
             function = "clear";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "c";
             function = "clearins";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "Esc";
             function = "quit";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "Home";
             function = "movestart";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "End";
             function = "moveend";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "g";
             function = "movestart";
             argument = "0";
           },
           { mode = 0;
             modifier = "Shift";
             key = "g";
             function = "moveend";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "Next";
             function = "movenext";
             argument = "0";
           },
           { mode = 0;
             modifier = "None";
             key = "Prior";
             function = "moveprev";
             argument = "0";
           },
           { mode = 0;
             modifier = "Alt";
             key = "p";
             function = "navhistory";
             argument = "-1";
           },
           { mode = 0;
             modifier = "Alt";
             key = "n";
             function = "navhistory";
             argument = "+1";
           },

           { ignoreglobalkeys = 1; } ),
};
