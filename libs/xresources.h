// This .Xresources array is read and compared to the xrdb. Simply add to the array if you need to.
ResourcePref resources[] = {
	{ "font",			             STRING,  &font },
    { "col_caretfg",                 STRING,  &col_caretfg },
    { "col_caretbg",                 STRING,  &col_caretbg },
    { "col_larrowfg",                STRING,  &col_larrowfg },
    { "col_rarrowfg",                STRING,  &col_rarrowfg },
    { "col_larrowbg",                STRING,  &col_larrowbg },
    { "col_rarrowbg",                STRING,  &col_rarrowbg },
    { "col_itemnormfg",              STRING,  &col_itemnormfg },
    { "col_itemnormbg",              STRING,  &col_itemnormbg },
    { "col_itemselfg",               STRING,  &col_itemselfg },
    { "col_itemselbg",               STRING,  &col_itemselbg },
    { "col_itemnormprifg",           STRING,  &col_itemnormprifg },
    { "col_itemnormpribg",           STRING,  &col_itemnormpribg },
    { "col_itemselprifg",            STRING,  &col_itemselprifg },
    { "col_itemselpribg",            STRING,  &col_itemselpribg },
    { "col_inputbg",                 STRING,  &col_inputbg },
    { "col_inputfg",                 STRING,  &col_inputfg },
    { "col_menu",                    STRING,  &col_menu },
	{ "col_numfg",                   STRING,  &col_numfg },
	{ "col_numbg",                   STRING,  &col_numbg },
	{ "col_modefg",                  STRING,  &col_modefg },
	{ "col_modebg",                  STRING,  &col_modebg },
	{ "col_hlnormfg",                STRING,  &col_hlnormfg },
	{ "col_hlnormbg",                STRING,  &col_hlnormbg },
	{ "col_hlselfg",                 STRING,  &col_hlselfg },
	{ "col_hlselbg",                 STRING,  &col_hlselbg },
    { "col_border",                  STRING,  &col_border },
    { "col_promptfg",                STRING,  &col_promptfg },
    { "col_promptbg",                STRING,  &col_promptbg },
    { "col_capsfg",                  STRING,  &col_capsfg },
    { "col_capsbg",                  STRING,  &col_capsbg },

    // Alpha
    { "alpha_itemnormfg",            INTEGER, &alpha_itemnormfg },
    { "alpha_itemnormbg",            INTEGER, &alpha_itemnormbg },
    { "alpha_itemselfg",             INTEGER, &alpha_itemselfg },
    { "alpha_itemselbg",             INTEGER, &alpha_itemselbg },
    { "alpha_itemnormprifg",         INTEGER, &alpha_itemnormprifg },
    { "alpha_itemnormpribg",         INTEGER, &alpha_itemnormpribg },
    { "alpha_itemselprifg",          INTEGER, &alpha_itemselprifg },
    { "alpha_itemselpribg",          INTEGER, &alpha_itemselpribg },
    { "alpha_inputfg",               INTEGER, &alpha_inputfg },
    { "alpha_inputbg",               INTEGER, &alpha_inputbg },
    { "alpha_menu",                  INTEGER, &alpha_menu },
    { "alpha_promptfg",              INTEGER, &alpha_promptfg },
    { "alpha_promptbg",              INTEGER, &alpha_promptbg },
    { "alpha_larrowfg",              INTEGER, &alpha_larrowfg },
    { "alpha_larrowbg",              INTEGER, &alpha_larrowbg },
    { "alpha_rarrowfg",              INTEGER, &alpha_rarrowfg },
    { "alpha_rarrowbg",              INTEGER, &alpha_rarrowbg },
    { "alpha_hlnormfg",              INTEGER, &alpha_hlnormfg },
    { "alpha_hlnormbg",              INTEGER, &alpha_hlnormbg },
    { "alpha_hlselfg",               INTEGER, &alpha_hlselfg },
    { "alpha_hlselbg",               INTEGER, &alpha_hlselbg },
    { "alpha_numfg",                 INTEGER, &alpha_numfg },
    { "alpha_numbg",                 INTEGER, &alpha_numbg },
    { "alpha_border",                INTEGER, &alpha_border },
    { "alpha_caretfg",               INTEGER, &alpha_caretfg },
    { "alpha_caretbg",               INTEGER, &alpha_caretbg },
    { "alpha_modefg",                INTEGER, &alpha_modefg },
    { "alpha_modebg",                INTEGER, &alpha_modebg },
    { "alpha_capsfg",                INTEGER, &alpha_capsfg },
    { "alpha_capsbg",                INTEGER, &alpha_capsbg },

    // SGR sequence colors
   	{ "col_sgr0",                    STRING,  &col_sgr0 },
	{ "col_sgr1",                    STRING,  &col_sgr1 },
	{ "col_sgr2",                    STRING,  &col_sgr2 },
	{ "col_sgr3",                    STRING,  &col_sgr3 },
	{ "col_sgr4",                    STRING,  &col_sgr4 },
	{ "col_sgr5",                    STRING,  &col_sgr5 },
	{ "col_sgr6",                    STRING,  &col_sgr6 },
	{ "col_sgr7",                    STRING,  &col_sgr7 },
	{ "col_sgr8",                    STRING,  &col_sgr8 },
	{ "col_sgr9",                    STRING,  &col_sgr9 },
	{ "col_sgr10",                   STRING,  &col_sgr10 },
	{ "col_sgr11",                   STRING,  &col_sgr11 },
	{ "col_sgr12",                   STRING,  &col_sgr12 },
	{ "col_sgr13",                   STRING,  &col_sgr13 },
	{ "col_sgr14",                   STRING,  &col_sgr14 },
	{ "col_sgr15",                   STRING,  &col_sgr15 },

    // General options
    { "promptpwlstyle",              INTEGER, &promptpwlstyle },
    { "matchcountpwlstyle",          INTEGER, &matchcountpwlstyle },
    { "modepwlstyle",                INTEGER, &modepwlstyle },
    { "capspwlstyle",                INTEGER, &capspwlstyle },
    { "powerlineprompt",             INTEGER, &powerlineprompt },
    { "powerlinecount",              INTEGER, &powerlinecount },
    { "powerlinemode",               INTEGER, &powerlinemode },
    { "powerlinecaps",               INTEGER, &powerlinecaps },
    { "dockproperty",                INTEGER, &dockproperty },
    { "globalcolors",                INTEGER, &globalcolors },
    { "coloritems",                  INTEGER, &coloritems },
    { "sgr",                         INTEGER, &sgr },
	{ "menuposition",                INTEGER, &menuposition },
    { "xpos",                        INTEGER, &xpos },
    { "ypos",                        INTEGER, &ypos },
    { "menuwidth",                   INTEGER, &menuwidth },
	{ "menupaddingv",                INTEGER, &menupaddingv },
	{ "menupaddingh",                INTEGER, &menupaddingh },
    { "menumarginv",                 INTEGER, &menumarginv },
    { "menumarginh",                 INTEGER, &menumarginh },
    { "textpadding",                 INTEGER, &textpadding },
    { "indentitems",                 INTEGER, &indentitems },
    { "accuratewidth",               INTEGER, &accuratewidth },
    { "alpha",                       INTEGER, &alpha },
    { "type",                        INTEGER, &type },
    { "passwd",                      INTEGER, &passwd },
    { "minwidth",                    INTEGER, &minwidth },
    { "preselected",                 INTEGER, &preselected },
	{ "borderwidth",                 INTEGER, &borderwidth },
	{ "lines",                       INTEGER, &lines },
    { "lineheight",                  INTEGER, &lineheight },
	{ "columns",                     INTEGER, &columns },
	{ "maxhist",                     INTEGER, &maxhist },
    { "hideitem",                    INTEGER, &hideitem },
    { "hidematchcount",              INTEGER, &hidematchcount },
    { "hidehighlight",               INTEGER, &hidehighlight },
    { "hidemode",                    INTEGER, &hidemode },
    { "hideimage",                   INTEGER, &hideimage },
    { "hidelarrow",                  INTEGER, &hidelarrow },
    { "hiderarrow",                  INTEGER, &hiderarrow },
    { "hideprompt",                  INTEGER, &hideprompt },
    { "hideinput",                   INTEGER, &hideinput },
    { "hidepowerline",               INTEGER, &hidepowerline },
    { "hidecaret",                   INTEGER, &hidecaret },
    { "hidecursor",                  INTEGER, &hidecaret },
    { "hidecaps",                    INTEGER, &hidecaps },
	{ "histdup",                     INTEGER, &histdup },
    { "casesensitive",               INTEGER, &casesensitive },
    { "imagewidth",                  INTEGER, &imagewidth },
    { "imageheight",                 INTEGER, &imageheight },
    { "imagegaps",                   INTEGER, &imagegaps },
    { "imageposition",               INTEGER, &imageposition },
    { "generatecache",               INTEGER, &generatecache },
    { "maxcache",                    INTEGER, &maxcache },
    { "mode",                        INTEGER, &mode },
    { "fast",                        INTEGER, &fast },
    { "managed",                     INTEGER, &managed },
    { "mon",                         INTEGER, &mon },
    { "sortmatches",                 INTEGER, &sortmatches },
    { "fuzzy",                       INTEGER, &fuzzy },
    { "pango_item",                  INTEGER, &pango_item },
    { "pango_prompt",                INTEGER, &pango_prompt },
    { "pango_input",                 INTEGER, &pango_input },
    { "pango_leftarrow",             INTEGER, &pango_leftarrow },
    { "pango_rightarrow",            INTEGER, &pango_rightarrow },
    { "pango_numbers",               INTEGER, &pango_numbers },
    { "pango_mode",                  INTEGER, &pango_mode },
    { "pango_caps",                  INTEGER, &pango_caps },
    { "pango_password",              INTEGER, &pango_password },
    { "pango_highlight",             INTEGER, &pango_highlight },
};

ResourcePref cols[] = {
	{ "color10",                     STRING,  &col_caretfg },
    { "color0",                      STRING,  &col_caretbg },
	{ "color4",                      STRING,  &col_larrowfg },
	{ "color4",                      STRING,  &col_rarrowfg },
	{ "color10",                     STRING,  &col_itemnormfg },
	{ "color10",                     STRING,  &col_itemnormprifg },
	{ "color10",                     STRING,  &col_inputfg },
	{ "color0",                      STRING,  &col_itemnormbg },
	{ "color0",                      STRING,  &col_itemnormpribg },
	{ "color0",                      STRING,  &col_menu },
	{ "color0",                      STRING,  &col_larrowbg },
	{ "color0",                      STRING,  &col_rarrowbg },
	{ "color0",                      STRING,  &col_itemselfg },
	{ "color0",                      STRING,  &col_itemselprifg },
	{ "color0",                      STRING,  &col_inputbg },
	{ "color12",                     STRING,  &col_promptbg },
	{ "color0",                      STRING,  &col_promptfg },
	{ "color7",                      STRING,  &col_capsbg },
	{ "color0",                      STRING,  &col_capsfg },
	{ "color6",                      STRING,  &col_itemselbg },
	{ "color6",                      STRING,  &col_itemselpribg },
	{ "color6",                      STRING,  &col_border },
	{ "color0",                      STRING,  &col_numfg },
	{ "color5",                      STRING,  &col_numbg },
	{ "color0",                      STRING,  &col_modefg },
	{ "color11",                     STRING,  &col_modebg },
	{ "color2",                      STRING,  &col_hlnormbg },
	{ "color3",                      STRING,  &col_hlselbg },
	{ "color0",                      STRING,  &col_hlnormfg },
	{ "color0",                      STRING,  &col_hlselfg },
   	{ "color0",                      STRING,  &col_sgr0 },
	{ "color1",                      STRING,  &col_sgr1 },
	{ "color2",                      STRING,  &col_sgr2 },
	{ "color3",                      STRING,  &col_sgr3 },
	{ "color4",                      STRING,  &col_sgr4 },
	{ "color5",                      STRING,  &col_sgr5 },
	{ "color6",                      STRING,  &col_sgr6 },
	{ "color7",                      STRING,  &col_sgr7 },
	{ "color8",                      STRING,  &col_sgr8 },
	{ "color9",                      STRING,  &col_sgr9 },
	{ "color10",                     STRING,  &col_sgr10 },
	{ "color11",                     STRING,  &col_sgr11 },
	{ "color12",                     STRING,  &col_sgr12 },
	{ "color13",                     STRING,  &col_sgr13 },
	{ "color14",                     STRING,  &col_sgr14 },
	{ "color15",                     STRING,  &col_sgr15 },
};
