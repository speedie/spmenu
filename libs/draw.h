// declare functions
static void drawmenu(void);
static int drawprompt(int x, int y, int w);
static int drawinput(int x, int y, int w);
static int drawnumber(int x, int y, int w);
static int drawmode(int x, int y, int w);
static int drawcaps(int x, int y, int w);
static int drawlarrow(int x, int y, int w);
static int drawrarrow(int x, int y, int w);
