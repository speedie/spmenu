static void init_appearance(void);

// Color schemes
enum { SchemeLArrow,
       SchemeRArrow,
       SchemeItemNorm,
       SchemeItemSel,
       SchemeItemNormPri,
       SchemeItemSelPri,
       SchemeMenu,
       SchemeInput,
       SchemePrompt,
       SchemeNormHighlight,
       SchemeSelHighlight,
       SchemeCaret,
       SchemeNumber,
       SchemeMode,
       SchemeBorder,
       SchemeCaps,
       SchemeLast,
};
